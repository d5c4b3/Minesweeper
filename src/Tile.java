public class Tile {
	public boolean isHidden;
	public boolean isMine;
	public boolean isFlagged;
	public int numAdjMines;
	public Tile() {
		this.isHidden = true;
		this.isMine = false;
		this.isFlagged = false;
		this.numAdjMines = 0;
	}
}