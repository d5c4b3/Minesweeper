import javax.swing.*;
import java.awt.*;
@SuppressWarnings("serial")
public class ScoreBoard extends JPanel {
	
	Game game = Game.getInstance();
	
	public ScoreBoard() {
		super(true);
		setBackground(Color.black);
		setMinimumSize(new Dimension(0, 50));
		setPreferredSize(new Dimension(0, 50));
	}

	public void paintComponent(Graphics g) {
		if (game == null) {
			Game.getInstance();
		}
		
		super.paintComponent(g);

		Graphics2D graphics2D = (Graphics2D) g;

		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON); 

	   // Set anti-alias for text
	    graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
	            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g.setColor(new Color(116, 125, 125));
		g.fillRoundRect(getWidth()/2 - 32, 5, 80, 40, 15, 15);

		g.setColor(Color.black);
		g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 32));
		int letterX = (getWidth()/2 - SwingUtilities.computeStringWidth(g.getFontMetrics(), game.getScore()+"")/2 + 6);
		int letterY = (getHeight()/2 + g.getFontMetrics().getHeight()/4);
		g.drawString(game.getScore()+"", letterX, letterY);
	}
}