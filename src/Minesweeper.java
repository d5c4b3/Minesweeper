public class Minesweeper {
	public static GameFrame gameFrame;
	public static void main(String args[]) {
		if (args.length == 3) {
			gameFrame = new GameFrame(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		} else {
			gameFrame = new GameFrame();
		}
	}
}