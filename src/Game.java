import java.util.Collections;
import java.util.Arrays;
public class Game {
	private Tile field[][];
	private int score = 0;
	private int fieldWidth;
	private int fieldHeight;
	private int numMines;
	private static Game _instance;

	private Game(int width, int height, int numMines) {
		fieldHeight = height;
		fieldWidth = width;
		this.numMines = numMines;
		field = initGrid(width, height, numMines);
	}

	public static Game getInstance(int width, int height, int numMines) {
		if (_instance == null) {
			_instance = new Game(width, height, numMines);
		}
		return _instance;
	}

	public static Game getInstance() {
		return _instance;
	}

	public static void restart(int width, int height, int numMines) {
		_instance = new Game(width, height, numMines);
	}

	public void revealMines() {
		for (int i = 0; i < field.length; i++) {
			for (int n = 0; n < field[i].length; n++) {
				if (field[i][n].isMine) {
					field[i][n].isHidden = false;
				}
			}
		}
	}

	public void revealTile(int x, int y) {
		boolean temp = field[x][y].isHidden;

		field[x][y].isHidden = false;

		if (field[x][y].numAdjMines == 0 && temp && !field[x][y].isMine) {
			if (x > 0) {

				if (y > 0) {
					revealTile(x-1, y-1);
				}

				revealTile(x-1, y);

				if (y < fieldHeight-1) {
					revealTile(x-1, y+1);
				}

			}

			if (y > 0) {
				revealTile(x, y-1);
			}

			if (y < fieldHeight-1) {
				revealTile(x, y+1);
			}

			if (x < fieldWidth-1) {

				if (y > 0) {
					revealTile(x+1, y-1);
				}

				revealTile(x+1, y);

				if (y < fieldHeight-1) {
					revealTile(x+1, y+1);
				}
			}
		}
	}

	public void toggleFlagTileAt(int x, int y) {
		field[x][y].isFlagged = !field[x][y].isFlagged;
	}

	public Tile[][] initGrid(int width, int height, int numMines) {
		Tile[] shuffledArray = new Tile[width*height];
		
		for (int i = 0; i < shuffledArray.length; i++) {
			shuffledArray[i] = new Tile();
		}

		for (int i = 0; (i < shuffledArray.length && i < numMines); i++) {
			shuffledArray[i].isMine = true;
		}

		Collections.shuffle(Arrays.asList(shuffledArray));

		Tile[][] grid = new Tile[width][height];
		int index = 0;
		for(int i = 0; i < grid.length; i++) {
			for (int n = 0; n < grid[i].length; n++) {
				grid[i][n] = shuffledArray[index];
				index++;
			}
		}

		for(int i = 0; i < grid.length; i++) {
			for (int n = 0; n < grid[i].length; n++) {
				if (grid[i][n].isMine) {
					try {grid[i-1][n-1].numAdjMines++;} catch (Exception e) {}
					try {grid[i-1][n].numAdjMines++;} catch (Exception e) {}
					try {grid[i-1][n+1].numAdjMines++;} catch (Exception e) {}
					try {grid[i][n-1].numAdjMines++;} catch (Exception e) {}
					try {grid[i][n+1].numAdjMines++;} catch (Exception e) {}
					try {grid[i+1][n-1].numAdjMines++;} catch (Exception e) {}
					try {grid[i+1][n].numAdjMines++;} catch (Exception e) {}
					try {grid[i+1][n+1].numAdjMines++;} catch (Exception e) {}
				}
			}
		}

		return grid;
	}

	public Tile getTileAt(int x, int y) {
		return field[x][y];
	}

	public int getHeight() {
		return fieldHeight;
	}

	public int getWidth() {
		return fieldWidth;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getNumMines() {
		return numMines;
	}
}
