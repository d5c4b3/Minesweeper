import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class GameFrame extends JFrame implements Runnable {

	//Game loop information
	private Thread thread;
	private boolean running = false;
	private int FPS = 60;
	private long targetTime = 1000/FPS;


	//Panels
	private GameMenuBar gameMenuBar;
	private ScoreBoard scoreBoard;
	private GamePanel gamePanel;

	//game info
	int fieldWidth;
	int fieldHeight;
	int numMines;

	public GameFrame(int width, int height, int mines) {

		fieldWidth = width;
		fieldHeight = height;
		numMines = mines;
		
		
		gamePanel = new GamePanel(fieldWidth, fieldHeight, numMines, this);
		gameMenuBar = new GameMenuBar();
		scoreBoard = new ScoreBoard();

		
		setTitle("Minesweeper by CD Silsby");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = c.gridy = 0;
		add(gameMenuBar, c);

		
		c.gridy = 1;
		add(scoreBoard, c);

		c.gridy = 2;
		c.weightx = c.weighty = 1.0;
		add(gamePanel, c);
		pack();
		setLocationByPlatform(true);
		setVisible(true);
	}

	public GameFrame() {
		this(9, 9, 10);
	}
	
	public void addNotify() {
		//sometimes the constructor is called after everything has intitialized
		//sometimes it doesn't this makes sure that we call this after it has
		super.addNotify();

		if (thread == null) {
			running = true;
			thread = new Thread(this);
			thread.start();
		}
	}

	public void run() {
		//This allows me to be sure that each of these is 
		//called exactly 60 times per second
		long time; //used to find the time it took to update (start time)
		long elapsed; //time it took to update
		long wait; //the targetTime - the amount of time it took to update and render

		while(running) {
			time = System.nanoTime();

			draw();

			elapsed = System.nanoTime() - time;
			wait = targetTime - elapsed / 1000000;

			if (wait < 0) {wait = 5;}

			try {
				Thread.sleep(wait);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void draw() {
		gamePanel.repaint();
		scoreBoard.repaint();
	}

	public void restart(int fieldWidth, int fieldHeight, int numMines) {
		dispose();
		gamePanel.restart(fieldWidth, fieldHeight, numMines);
		new GameFrame(fieldWidth, fieldHeight, numMines);
	}
	public void stop() {
		running = false;
	}
}