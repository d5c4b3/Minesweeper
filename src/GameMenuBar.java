import javax.swing.*;
import java.awt.event.*;
@SuppressWarnings("serial")

public class GameMenuBar extends JMenuBar {
	public GameMenuBar() {
		JMenu fileMenu = new JMenu("Difficulty");
	    fileMenu.setMnemonic(KeyEvent.VK_D);
	    add(fileMenu);

	    JMenuItem beginner = new JMenuItem(new AbstractAction("Beginner") {
		    public void actionPerformed(ActionEvent e) {
		        GameFrame frame = (GameFrame) getTopLevelAncestor();
		        frame.restart(9, 9, 10);
		    }
		});
	    fileMenu.add(beginner);

	    JMenuItem intermediate = new JMenuItem(new AbstractAction("Intermediate") {
		    public void actionPerformed(ActionEvent e) {
		        GameFrame frame = (GameFrame) getTopLevelAncestor();
		        frame.restart(16, 16, 40);
		    }
		});
	    fileMenu.add(intermediate);

	    JMenuItem expert = new JMenuItem(new AbstractAction("Expert") {
		    public void actionPerformed(ActionEvent e) {
		        GameFrame frame = (GameFrame) getTopLevelAncestor();
		        frame.restart(30, 16, 99);
		    }
		});
	    fileMenu.add(expert);
	}
}