import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
@SuppressWarnings("serial")
public class GamePanel extends JPanel implements MouseListener {
	int TILE_SIZE = 32;
	int fieldWidth;
	int fieldHeight;
	int numMines;
	BufferedImage flaggedTile;
	BufferedImage hiddenTile;
	BufferedImage mineTile;
	BufferedImage shownTile;
	Game game;
	GameFrame frame;

	public GamePanel(int fieldWidth, int fieldHeight, int numMines, GameFrame frame) {
		game = Game.getInstance(fieldWidth, fieldHeight, numMines);

		this.fieldHeight = fieldHeight;
		this.fieldWidth = fieldWidth;
		this.numMines = numMines;
		
		this.frame = frame;

		addMouseListener(this);

		try {
		    flaggedTile = ImageIO.read(getClass().getResourceAsStream("/resources/flaggedTile.png"));
		    hiddenTile  = ImageIO.read(getClass().getResourceAsStream("/resources/hiddenTile.png"));
		    mineTile    = ImageIO.read(getClass().getResourceAsStream("/resources/mineTile.png"));
		    shownTile   = ImageIO.read(getClass().getResourceAsStream("/resources/shownTile.png"));
		} catch (IOException e) {
			System.out.println("Error fetching images");
		}
		setPreferredSize(new Dimension(TILE_SIZE * fieldWidth, TILE_SIZE * fieldHeight));
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D graphics2D = (Graphics2D) g;

		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON); 

	   // Set anti-alias for text
	    graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
	            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		for (int i = 0; i < fieldWidth; i++) {
			for (int n = 0; n < fieldHeight; n++) {
				Tile tile = game.getTileAt(i, n);
				if (tile.isFlagged && tile.isHidden) {
					g.drawImage(flaggedTile, i*TILE_SIZE, n*TILE_SIZE, null);
				} else if (tile.isHidden) {
					g.drawImage(hiddenTile, i*TILE_SIZE, n*TILE_SIZE, null);
				} else if (tile.isMine) {
					g.drawImage(mineTile, i*TILE_SIZE, n*TILE_SIZE, null);
				} else {
					g.drawImage(shownTile, i*TILE_SIZE, n*TILE_SIZE, null);
					if (tile.numAdjMines > 0) {
						g.setFont(new Font(Font.MONOSPACED, Font.BOLD,(int)(TILE_SIZE * 0.5)));
						int letterX = (i*TILE_SIZE + TILE_SIZE/2 - g.getFontMetrics().stringWidth(tile.numAdjMines+"")/2);
						int letterY = (n*TILE_SIZE + TILE_SIZE/2 + g.getFontMetrics().getHeight()/4);
						g.drawString(tile.numAdjMines+"", letterX, letterY);
					}
				}
			}
		}
	}

	public Game getGame() {
		return game;
	}

	
	public void mouseReleased(MouseEvent e) {
		int tileX = (int)Math.floor(e.getX()/TILE_SIZE);
		int tileY = (int)Math.floor(e.getY()/TILE_SIZE);

		if(e.getButton() == MouseEvent.BUTTON1) {
			game.revealTile(tileX, tileY);
			if (game.getTileAt(tileX, tileY).isMine) {
				doGameOver();
			}
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			game.toggleFlagTileAt(tileX, tileY);
		}
		
		checkForWin();
		updateScore();
	}

	public void resize() {
		this.fieldHeight = game.getHeight();
		this.fieldWidth = game.getWidth();
		this.numMines = game.getNumMines();
		setPreferredSize(new Dimension(TILE_SIZE * fieldWidth, TILE_SIZE * fieldHeight));
	}
	
	private void checkForWin() {
		game = Game.getInstance();
		boolean win = true;
		
		//We have won if and only if every mine has a flag and all other tiles don't
		breakLabel:
		for (int i = 0; i < fieldWidth; i++) {
			for (int n = 0; n < fieldHeight; n++) {
				Tile tile = game.getTileAt(i, n);
				if ((tile.isMine && !tile.isFlagged) || (!tile.isMine && tile.isFlagged)) {
					win = false;
					break breakLabel;
				}
			}
		}
		
		//If we have won ask to play again
		if (win) {
			doWin();
		}
	}
 
	public void restart(int fieldWidth, int fieldHeight, int numMines) {
		Game.restart(fieldWidth, fieldHeight, numMines);
	}
	
	private void doWin() {
		repaint();
		frame.stop();
		showPlayAgainDialog(true);
	}
	
	private void doGameOver() {
		game.revealMines();
		repaint();
		frame.stop();
		showPlayAgainDialog(false);
	}
	
	private void showPlayAgainDialog(boolean win) {
		String title;
		String message;
		if (win) {
			title = "You Win!";
			message = "You Won! Would you like to play again?";
		} else {
			title = "You lost!";
			message = "You lost... Would you like to play again?";
		}
		
		Object[] options = {"Yes, please", "No, thanks"};
		int n = JOptionPane.showOptionDialog(
			frame,
    		message,
    		title,
    		JOptionPane.YES_NO_CANCEL_OPTION,
    		JOptionPane.QUESTION_MESSAGE,
    		null,
    		options,
    		options[0]
    	);
		
		if (n == 0) {
			frame.restart(fieldWidth, fieldHeight, numMines);
		} else {
			System.exit(1);
		}
	}
	
	private void updateScore() {
		if (game != null) {
			int numFlagsPlaced = 0;
			for (int i = 0; i < game.getWidth(); i++) {
				for (int n = 0; n < game.getHeight(); n++) {
					Tile tile = game.getTileAt(i, n);
					if (tile.isFlagged && tile.isHidden) {
						numFlagsPlaced++;
					}
				}
			}
			game.setScore(game.getNumMines() - numFlagsPlaced);
		}
	}
	
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mousePressed(MouseEvent e) {
	}
}